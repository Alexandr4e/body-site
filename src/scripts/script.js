$(document).ready(function() {
	$('body').addClass('loaded');


	//= parts/01.mobile-menu.js
	//= parts/02.header-dropdowns.js
	//= parts/03.title-line-dropdown.js
	//= parts/04.sliders.js
	//= parts/05.checkbox.js
	//= parts/06.upload-file.js
	//= parts/07.city-choice.js
	//= parts/08.selector.js
	//= parts/10.range-slider.js
	//= parts/11.order-tabs.js
	//= parts/12.user-dropdown.js
	//= parts/13.popup.js
	//= parts/14.mobile-menu-tabs.js
});
