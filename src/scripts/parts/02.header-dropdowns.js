/**
 * Open dropdowns in header
 */

(function() {
	var $link = $('.js-dropdown-link'),
		$body = $("body"),
		$header = $('#header'),
		ww = $(window).width();

	if (ww <= 900) {
		$link.click(function() {
			var $t = $(this),
				id = $t.data('id'),
				$dropdown = $('#bd-dropdown_' + id);

			$dropdown.toggleClass('bd-dropdown_active');
			$header.toggleClass('menu-open');
			$body.toggleClass('bd-dropdown-open');

			return false;
		});
	}
})();