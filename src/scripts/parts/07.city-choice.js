/**
 * City choice
 */

(function() {
	var $btn = $('.js-contacts-city'),
		$location = $('.js-contacts-location'),
		activeCityClass = 'contacts__city_active',
		activeLocationClass = 'contacts__location_active',
		$mBtn = $('#contacts-city-mobile');

	$mBtn.change(function() {
		var $t = $(this),
			id = $t.val(),
			$currentLocation = $('#contacts-location_' + id);


		$btn.removeClass(activeCityClass);
		$t.addClass(activeCityClass);

		$location.removeClass(activeLocationClass);
		$currentLocation.addClass(activeLocationClass);

		return false;

	});

	$btn.click(function() {
		var $t = $(this),
			id = $t.data('id'),
			$currentLocation = $('#contacts-location_' + id);

		$btn.removeClass(activeCityClass);
		$t.addClass(activeCityClass);

		$location.removeClass(activeLocationClass);
		$currentLocation.addClass(activeLocationClass);

		return false;
	} );

}());