/**
 * Open dropdowns in title-line
 */

(function() {
	var ww = $(window).width(),
		$body = $("body"),
		$header = $('#header');

	if (ww <= 600) {
		var $link = $('.js-dropdown-trigger');

		$link.click(function() {
			var $t = $(this),
				$dropdown = $t.find('.js-menu-dropdown');

			$link.toggleClass('title-line_open');
			$t.toggleClass('title-line_active');
			$header.toggleClass('menu-open');
			$dropdown.toggleClass('title-line__dropdown_active');
			$body.toggleClass('bd-dropdown-open');
		});
	}
})();