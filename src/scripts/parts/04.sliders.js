/**
 * Sliders
 */

//Main slider
(function() {
	$('.js-main-slider').slick({

	});
})();

//Why us slider
(function() {
	var ww = $(window).width();

	if (ww <= 900) {
		$('.js-about-page-slider').slick({
			arrows: false,
			dots: true,
			slidesToShow: 1,
			slidesToScroll: 1,
		});
	}
})();


//Advantages response slider
(function() {
	var ww = $(window).width();

	if (ww <= 900) {
		$('.js-advantages-response-slider').slick({
			arrows: false,
			dots: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			infinite: false,
			dotsClass: 'std-dots',
		});
	}
})();