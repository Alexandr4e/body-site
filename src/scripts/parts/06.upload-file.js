/**
 * Add file
 */

(function() {
	var $button = $('.js-file-button');

	$button.click(function() {
		var $t = $(this),
			$input = $t.next();

		$input.click();

		return false;
	});

	function handleFileSelect(evt, $t) {
		var file = evt.target.files[0],
			$parent = $t.closest('.js-add-file-wrap'),
			$value = $parent.find('.js-file-name');

		// Only process image files.
		if (!file.type.match('image.*')) {
			return false;
		}

		$value.text(file.name);
	}

	// document.getElementById('upload-loyal-popup__upload-button').addEventListener('change', handleFileSelect, false);
	$('.js-file-input').change(function(e) {
		console.log(e);
		handleFileSelect(e, $(this));
	});
})();