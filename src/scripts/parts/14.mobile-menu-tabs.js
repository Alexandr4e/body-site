/**
 * Mobile menu tabs
 */

(function($) {
	var $tab = $('.js-menu-tab'),
		$content = $('.js-menu-content');

	$tab.click(function() {
		var $t = $(this),
			id = $t.data('id'),
			$currentContent = $('#menu-content_' + id);

		$content.removeClass('order__content_active');
		$currentContent.addClass('order__content_active');

		$tab.removeClass('order__tab-item_active');
		$t.addClass('order__tab-item_active');

		return false;
	});
})(jQuery);