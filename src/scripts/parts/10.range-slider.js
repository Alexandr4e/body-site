/**
 * Range slider in mobile menu
 */

(function($) {
	var handle = $('.js-custom-handle');

	Number.prototype.formatMoney = function(c, d, t){
		var n = this,
			c = isNaN(c = Math.abs(c)) ? 2 : c,
			d = d == undefined ? "." : d,
			t = t == undefined ? "," : t,
			s = n < 0 ? "-" : "",
			i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
			j = (j = i.length) > 3 ? j % 3 : 0;
		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	};


	$('.js-range-slider').slider({
		range: "min",
		min: 10000,
		max: 1000000,
		slide: function(event, ui) {
			var $tooltip = $(ui.handle).find('.form__range-slider-tooltip'),
				value = ui.value.formatMoney(0, '.', ' ') +  ' руб.',
				$parent = $tooltip.closest('.js-range-slider'),
				tooltipWidth,
				tooltipLeft,
				sliderLeft = $parent.offset().left,
				sliderWidth = $parent.width(),
				marginLeft;

			$tooltip.text(value);

			tooltipWidth = $tooltip.width();
			marginLeft = tooltipWidth / 2;
			tooltipLeft = $(ui.handle).offset().left;

			if (sliderLeft > tooltipLeft - 50) {
				$tooltip.css({
					marginLeft: 0
				});
			}
			else if (sliderLeft + sliderWidth < tooltipLeft + 100) {
				$tooltip.css({
					marginLeft: -tooltipWidth
				});
			}
			else {
				$tooltip.css({
					marginLeft: -marginLeft
				});
			}

		},
		create: function(event, ui) {
			var $tooltip = $('<div class="form__range-slider-tooltip"></div>');

			$(event.target).find('.ui-slider-handle').append($tooltip);
		},
	});
})(jQuery);