/**
 * User dropdown
 */

(function($) {
	var $link = $('.js-user-dropdown-link'),
		$wrap = $('#all-wrap'),
		$header = $('#header'),
		ww = $(window).width();

	if (ww > 900) {
		$link.click(function() {
			var $t = $(this),
				$parent = $t.closest('.js-user-dropdown-link-parent'),
				$drodpown = $parent.find('.js-user-dropdown');

			$drodpown.toggleClass('user-dropdown_open');
			$header.toggleClass('user-dropdown-open');

			$wrap.click(function() {
				$drodpown.removeClass('user-dropdown_open');
				$header.removeClass('user-dropdown-open');
			});
			return false;
		});
	}

})(jQuery);