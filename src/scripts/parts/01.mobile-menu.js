/**
 * Mobile menu open
 */

(function($) {
	var $burger = $('.js-header-burger');

	$burger.click(function() {
		var $menu = $('#mobile-menu'),
			$body = $("body"),
			$close = $('.js-menu-close');

		$menu.toggleClass('menu_active');
		$burger.toggleClass('header__burger_active');
		$body.toggleClass('menu-open');
		$close.click(function() {
			$menu.removeClass('menu_active');
			$body.removeClass('menu-open');
			$burger.removeClass('header__burger_active');
			return false;
		});

		return false;
	});
})(jQuery);